<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\ORM\Table;
use \Datetime;

/**
 * Register Controller
 *
 *
 * @method \App\Model\Entity\Register[] paginate($object = null, array $settings = [])
 */
class RegisterController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loginModel = TableRegistry::get('Login');


        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
    }

    public function saveRegisterDetails() {

        if (!empty($_REQUEST)) {


            if ($_REQUEST['password'] != $_REQUEST['password_confirmation']) {

                $data['messageType'] = 'FAILURE';
                $data['message'] = 'Password does not match';
                return $this->redirect(array('controller' => 'register/register?messageType=' . $data['messageType'] . '&message=' . $data['message']));
            }
            $customerData = $this->loginModel->validate($_REQUEST['email']);

            if ($customerData) {
                //Send a message saying already registered memeber.
                $data['messageType'] = 'FAILURE';
                $data['message'] = 'Email Id is alread registered.';
                return $this->redirect(array('controller' => 'register/register?messageType=' . $data['messageType'] . '&message=' . $data['message']));
            } else {
                $data = $this->loginModel->saveData($_REQUEST);
                if ($data) {
                    if (!isset($_SESSION)) {
                        session_start();
                    }
                    setcookie('userId', $data->id, time() + (86400 * 30), "/"); // 86400 = 1 day
                    $_SESSION['userId'] = $data->id;

                    return $this->redirect(array('controller' => 'Chat', 'action' => 'home'));
                } else {

                    $data['messageType'] = 'FAILURE';
                    $data['message'] = 'Registration failed. Please check your details again';
                    return $this->redirect(array('controller' => 'register/register?messageType=' . $data['messageType'] . '&message=' . $data['message']));
                }
            }
        }
    }

    public function register() {
        
    }

}
