<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use \Datetime;

/**
 * Chat Controller
 *
 *
 * @method \App\Model\Entity\Chat[] paginate($object = null, array $settings = [])
 */
class ChatController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->chatsModel = TableRegistry::get('Chats');
        $this->loginModel = TableRegistry::get('Login');



        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $chat = $this->paginate($this->Chat);

        $this->set(compact('chat'));
        $this->set('_serialize', ['chat']);
    }

    public function home() {

        if (!empty($_COOKIE['userId'])) {
            $userId = $_COOKIE['userId'];

            $customerData = $this->loginModel->getDetails($userId);
            if (!empty($customerData)) {

                $allUsers = $this->loginModel->allUsers();

                if (!empty($allUsers)) {
                    $this->set('allUsers', $allUsers);
                }

                $chats = $this->chatsModel->receivedChat($userId);
                
                $sentChat = $this->chatsModel->sentChat($userId);

                if (!empty($chats)) {
                    $this->set('chats', $chats);
                }
                if(!empty($sentChat)){
                    
                    $this->set('sentChat', $sentChat);
                }

                $this->set('customerDetails', $customerData);
            }
        }
    }

    public function saveMessage() {

        if (!empty($_COOKIE['userId'])) {
            $userId = $_COOKIE['userId'];

            $requestData = $_REQUEST;

            $receiverDetails = $this->loginModel->getDetails($requestData['receiverId']);
            $requestData['receiver'] = $receiverDetails->first_name;
            $data = $this->chatsModel->saveChat($userId, $requestData);

            if($data){
                
                return $this->redirect(array('controller' => 'Chat', 'action' => 'home?messageType=SUCCESS'));

            } else {
                
                return $this->redirect(array('controller' => 'Chat', 'action' => 'home?messageType=FAILURE'));

            }
        }
    }

}
