<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use \Datetime;

/**
 * Login Model
 *
 * @method \App\Model\Entity\Login get($primaryKey, $options = [])
 * @method \App\Model\Entity\Login newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Login[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Login|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Login patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Login[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Login findOrCreate($search, callable $callback = null, $options = [])
 */
class LoginTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('login');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->requirePresence('id', 'create')
                ->notEmpty('id');

        $validator
                ->email('email')
                ->allowEmpty('email');

        $validator
                ->scalar('password')
                ->allowEmpty('password');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }

    public function getDetails($id) {

        if (isset($id) && !empty($id)) {
            $customerDetails = $this->find()
                    ->where(['id' => $id])
                    ->first();
            return ($customerDetails);
        } else {

            return false;
        }
        return $customerDetails;
    }

    public function validate($email) {

        if (isset($email) && !empty($email)) {
            $customerDetails = $this->find()
                    ->where(['email' => $email])
                    ->first();
            return ($customerDetails);
        }

        return false;
    }

    public function allUsers() {
        
        $allUsers = $this->find()
                ->all();
        if(!empty($allUsers)){
            
            return ($allUsers);
        } else {
            
            return false;
        }
    }

    public function saveData() {

        $loginTable = TableRegistry::get('Login');
        $user = $loginTable->newEntity();

        $user->first_name = $_REQUEST['first_name'];
        $user->last_name = $_REQUEST['last_name'];
        $user->email = $_REQUEST['email'];
        $user->password = $_REQUEST['password'];

        $savedData = $loginTable->save($user);

        return ($savedData);
    }
    
}
