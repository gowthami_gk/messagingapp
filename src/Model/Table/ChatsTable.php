<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use \Datetime;

/**
 * Chats Model
 *
 * @property \App\Model\Table\SendersTable|\Cake\ORM\Association\BelongsTo $Senders
 * @property \App\Model\Table\ReceiversTable|\Cake\ORM\Association\BelongsTo $Receivers
 *
 * @method \App\Model\Entity\Chat get($primaryKey, $options = [])
 * @method \App\Model\Entity\Chat newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Chat[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Chat|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Chat patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Chat[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Chat findOrCreate($search, callable $callback = null, $options = [])
 */
class ChatsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('chats');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    public function saveChat($userId, $requestDetails) {


        $chatsTable = TableRegistry::get('Chats');
        $chat = $chatsTable->newEntity();

        $chat->sender_id = $userId;
        $chat->receiver_id = $requestDetails['receiverId'];
        $chat->sender_name = $requestDetails['sender'];
        $chat->receiver_name = $requestDetails['receiver'];
        $chat->message = $requestDetails['message'];
        $chat->created_at = date('Y-m-d H:i:s');;

        $savedData = $chatsTable->save($chat);
        return $savedData;
    }

    public function receivedChat($receiverId) {

        if (isset($receiverId) && !empty($receiverId)) {
            $chats = $this->find()
                    ->where(['receiver_id' => $receiverId])
                    ->all();
            return ($chats);
        }

        return false;
    }
    
    public function sentChat($senderId){
        
        if (isset($senderId) && !empty($senderId)) {
            $chats = $this->find()
                    ->where(['sender_id' => $senderId])
                    ->all();
            return ($chats);
        }

        return false;
        
    }

}
