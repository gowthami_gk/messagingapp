

<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>

    <h2 style="color:skyblue"><center>Hello 
            <?php echo $customerDetails->first_name;
            ?>
        </center>
    </h2>


    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">

            <input class="btn-danger" style="float:right" type="submit" onclick="window.location.href = '/login/logout/'" value="Logout">

        </div>
    </div>
    <?php

    if(isset($_REQUEST['message-type'])){
    if( $_REQUEST['message-type'] == 's-u-c-c-e-s-s'){ ?>
<div>
    <div class="row">
            <div class="form-group">
                <div  style="margin:10px;padding:5px;">

                    <p style ="color:green"> <?php echo "message sent"; ?> </p>
                </div>
            </div>

        <?php  } else { ?>

            <div class="form-group">
                <div  style="margin:10px;padding:5px;">

                    <p style ="color:red"> <?php echo "message not sent"; ?> </p>
                </div>
            </div>
    </div>
</div>


    <?php }
    }
    ?>

    <div>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#sendMessage">Send message</a></li>
        <li><a data-toggle="tab" href="#receivedMessages">Received messages</a></li>
        <li><a data-toggle="tab" href="#sentMessages">Sent messages</a></li>
    </ul>
    <div class="tab-content">
        <div id="sendMessage" class="tab-pane fade in active">
            <div class="table-responsive">
                <table style="width:100%">
                    <tr>
                        <th>Name</th>
                        <th>Message</th> 
                        <th></th> 

                    </tr>

                    <?php
                    if(!empty($allUsers)){
                    foreach($allUsers as $user){  
                    ?>   

                    <tr>
                    <form action="/chat/saveMessage?receiverId=<?php echo $user->id; ?>&sender=<?php echo $user->first_name ?>" method="post" enctype="multipart/form-data" name="register">

                        <td>
                            <?php print $user->first_name .  print $user->last_name ?>
                        </td>
                        <td>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="message"  class="form-control input-sm" value="HI" placeholder="First Name" readonly="">
                                </div>
                            </div>

                        </td>
                        <td>


                            <input class="btn-info" type="submit" value="Send">

                        </td> 
                    </form>
                    </tr>
                    <?php }
                    }
                    ?>

                </table>

            </div>
        </div>
        <div id ="receivedMessages" class="tab-pane fade">
            <div class="table-responsive">
                <table class="table table-condensed">
                    <table style="width:100%">
                        <tr>
                            <th>Sender</th>
                            <th>Message</th> 
                            <th>Date</th> 

                        </tr>

                        <?php
                        if(!empty($chats)){



                        foreach($chats as $instanceChat){ ?>


                        <tr>
                            <td><?php  echo ($instanceChat->sender_name) ?></td>
                            <td> <?php echo ($instanceChat->message)?></td> 
                            <td> <?php echo ($instanceChat->created_at)?></td> 
                        </tr>
                        <?php }
                        }
                        ?>

                    </table>
                </table>
            </div>
        </div>
        <div id ="sentMessages" class="tab-pane fade">
            <div class="table-responsive">
                <table class="table table-condensed">
                    <table style="width:100%">
                        <tr>
                            <th>Sent to</th>
                            <th>Message</th> 
                            <th>Date</th> 

                        </tr>

                        <?php
                        if(!empty($sentChat)){



                        foreach($sentChat as $instanceChat){ ?>


                        <tr>
                            <td><?php  echo ($instanceChat->receiver_name) ?></td>
                            <td> <?php echo ($instanceChat->message)?></td> 
                            <td> <?php echo ($instanceChat->created_at)?></td> 
                        </tr>
                        <?php }
                        }
                        ?>

                    </table>
                </table>
            </div>
        </div>
    </div>
    </div>


