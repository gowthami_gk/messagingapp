<html>
   <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    
        <body>
        
        <div style ="margin-top: 200px" >
            
            <?php
            
            if(isset($_REQUEST['message-type'])){
            if( $_REQUEST['message-type'] == 'f-a-i-l-u-r-e'){ ?>
            
                     <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <div  style="margin:10px;padding:5px;">
                                   <?php if(isset($_REQUEST['message'])){
                                   
                                   ?>
                                   <p style ="color:red"> <?php echo $_REQUEST['message']; ?> </p>
                                 <?php  } ?>
                                </div>
                            </div>
                        </div>
                    </div>
            
          <?php  }
               }
            ?>
           <form action="/register/saveRegisterDetails"  method="post" enctype="multipart/form-data" name="register">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <input type="text" name="first_name" id="first_name" class="form-control input-sm" placeholder="First Name" required>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <input type="text" name="last_name" id="last_name" class="form-control input-sm" placeholder="Last Name" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class ="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <input type="email" name="email" id="email" class="form-control input-sm" placeholder="Email Address" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <input type="password" name="password" id="password" class="form-control input-sm" placeholder="Password" required>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-sm" placeholder="Confirm Password" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class ="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <input type="submit" value="Register" class="btn btn-info btn-block">
                    </div>
                 </div>
            </div>
        </form>

        </div>

        Already have an account?, please Login here. 
        
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <button class="btn btn-info btn-block" onclick="window.location.href='/login/'">Login</button>
                </div>
            </div>
        </div>

        
    </body>
    
    
    
</html>

        